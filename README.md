#MESOSPHERE

SETUP

Requirements:

- NodeJS & NPM
  - Install from: https://www.npmjs.com/get-npm


Running the App:
- $npm install
- $npm run dev


Third party libraries used:
- JS
  - JQuery 3.3.1
  - Mustache 2.3.0
- CSS
  - reset 2.0 (http://meyerweb.com/eric/tools/css/reset/)
