'use strict';

$(function(){
	var defaultActiveServers = 4;
	var appsPerServer = 2;
	var minute = (1000 * 60);
	var delay = 1000;
	var prefixServerId = 's-';
	var templateServer = $('#templateServer').html();
	var templateApp = $('#templateApp').html();
	var templateAppLoader = $('#templateAppLoader').html();
	var templateDestroySever = $('#templateDestroySever').html();
	var serverList = [];
	var appServers = {};
	var appQueue = [];
	var startingApp = false;
	//var destroyingServer = false;
	var destroyingServerId;
	Mustache.parse(templateServer); 
	Mustache.parse(templateAppLoader); 
	Mustache.parse(templateDestroySever); 

	setInterval(setTime, minute)
	
	$('.pageIndex').on('click', '.jsAddServer', onClickAddServerHandler);
	$('.pageIndex').on('click', '.jsDestroyServer', onClickDestroyServerHandler);
	$('.pageIndex').on('click', '.jsAddApp', onClickAddAppHandler);
	$('.pageIndex').on('click', '.jsRemoveApp', onClickRemoveAppHandler);
	$('.pageIndex').on('mouseenter', '.jsServer', onMouseEnterServerHandler);
	$('.pageIndex').on('mouseleave', '.jsServer', onMouseLeaveServerHandler);
	$('.pageIndex').on('click', '.jsDestroyOverlay', onClickDestroyOverlayHandler);
	

	//Handlers
	function onClickAddServerHandler(e) {
		e.preventDefault();
		hideMessage();
		addNewServer();
	}

	function onClickDestroyServerHandler(e) {
		e.preventDefault();
		hideMessage();
		destroyServer();
	}

	function onClickAddAppHandler(e) {
		e.preventDefault();
		hideMessage();
		addAppToQueue($(this));
	}

	function onClickRemoveAppHandler(e) {
		e.preventDefault();
		hideMessage();
		removeApp($(this));
	}

	function onMouseEnterServerHandler(e) {
		e.preventDefault();
		var rendered = Mustache.render(templateDestroySever);
		$(this).append(rendered)
	}

	function onMouseLeaveServerHandler(e) {
		e.preventDefault();
		$(this).find('.jsDestroyOverlay').remove();
	}

	function onClickDestroyOverlayHandler(e) {
		e.preventDefault();
		var $serverWrapper = $(this).closest('.jsServer');
		var serverId = $serverWrapper.attr('id');
		destroyServer(serverId);
	}
	//End Handlers

	function addNewServer() {
		var serverid = generateServerId();
		var rendered = Mustache.render(templateServer, {serverid: serverid});
		$('.jsDashboard').append(rendered);

		serverList.push({
			id: serverid,
			apps: 0
		});
	}

	function addAppToQueue($addBtn, timestamp) {
		var $eleParent = $addBtn.closest('.jsApp');
		var prop = $eleParent.data('prop');
		var data = $.extend({}, prop, {timestamp: timestamp});
		appQueue.push(data);
		executeQueue();
	}

	//Creates Apps stored in the Queue
	function executeQueue() {
		if(!startingApp && appQueue.length) {
			startingApp = true;
			var serverInfo = getFreeServerInfo();
			var serverId, appNumber;

			if(serverInfo == null) {
				displayMessage('No free servers found');
				startingApp = false;
				appQueue=[];//empty queue
				return false;
			} else {
				serverId = serverInfo['serverId'];
				appNumber = serverInfo['appNumber'];
				var qData = appQueue[0];
				var data = {
					id: qData['id'],
					abbr: qData['abbr'],
					title: qData['title'],
					timestamp: (qData['timestamp']) ? qData['timestamp'] : Date.now()
				}
				renderAppTemplate(serverId, data, appNumber);
				if(qData['timestamp']) {
					setTime();
				}
			}
			appQueue.shift();
		}
	}

	function removeApp($that) {
		var $eleParent = $that.closest('.jsApp');
		var prop = $eleParent.data('prop');
		var appId = prop.id;
		var servers = appServers[appId]['serverIds'];

		if(servers.length) {
			var serverId = servers[servers.length-1];
			var $server = $('#' + serverId);
			$server.find('.' + appId).last().remove();
			updateAppData(serverId, prop), false;
			updateServerData(serverId, false);
			resetServerTemplate($server);
		}
	}

	function updateServerData(serverId, isAdd) {
		var index = getServerIndex(serverId);
		if(isAdd) {
			serverList[index]['apps'] = serverList[index]['apps'] + 1;
		} else {
			serverList[index]['apps'] = serverList[index]['apps'] - 1;
		}
	}

	function updateAppData(serverId, appData, isAdd) {
		var appId = appData['id'];

		if(typeof appServers[appId] == 'undefined') {
			initAppServer(appId);
		}
		if(isAdd) {
			appServers[appId]['serverIds'].push(serverId);
		} else {
			appServers[appId]['serverIds'].pop();
		}
	}

	function resetServerTemplate($server) {
		if($server.hasClass('half')) {
			$server.removeClass('half');
		}
	}

	function renderAppTemplate(serverId, data, appNumber) {
		var $server = $('#' + serverId);
		var loader = Mustache.render(templateAppLoader, data);
		if(appNumber == 2) {
			$server.addClass('half');
		} else {
			$server.removeClass('half');
		}
		$server.append(loader);

		var renderTemplate = function() {
			$server.find('.jsAppLoader').remove();
			var rendered = Mustache.render(templateApp, data);
			$server.append(rendered);
			updateServerData(serverId, true);
			updateAppData(serverId, data, true);
			startingApp = false;
			executeQueue();
		};
		
		window.setTimeout(renderTemplate, delay);
	}

	function destroyServer(serverId) {
		if(!serverId) {
			serverId = getLastServerId();
		}
		//if no servers are left to destroy
		if(!serverId) return false;

		if(destroyingServerId==serverId) {
			displayMessage('Please try again. Server is being DESTROYED');
			return false
		} else {
			destroyingServerId = serverId;
		}
		var $server = $('.jsDashboard').find('#' + serverId);
		//This tries to redirect existing server apps to new server
		if(redirectAppsToNewServer($server)) {
			$server.remove();
			var index = getServerIndex(serverId);
			serverList.splice(index, 1);
			destroyingServerId = null;
		}
	}

	function activateDefaultActiveServers() {
		for(var i=0; i< defaultActiveServers; i++) {
			addNewServer();
		}
	}

	function redirectAppsToNewServer($server) {
		var apps = getServerActiveApps($server);
		var serverId = $server.attr('id');
		for(var i=0; i<apps.length; i++) {
			//Checks to see if any servers are free to receive the App
			if(getFreeServerInfo(serverId) != null) {
				var prop = apps[i];
				var $btn = $('.jsAppsBtn').find('#' + prop['id'] + ' .jsAddApp');
				addAppToQueue($btn, prop['timestamp']);
			}
		}
		return true;
	}

	function getFreeServerInfo(inactiveServerId) {
		var serverId = null;
		var appNumber = 0;
		for(var i=0; i < appsPerServer; i++) {
			appNumber = (i+1);
			for(var j=0; j< serverList.length; j++) {
				//Make sure inactiveServerId is not selected as free server
				if(inactiveServerId && inactiveServerId == serverList[j]['id']) {
					continue;
				}
				if(serverList[j]['apps'] < appNumber) {
					serverId = serverList[j]['id'];
					break;
				}
			}
			if(serverId !== null) break;
		}
		if(serverId != null) {
			return {serverId: serverId, appNumber: appNumber};
		} else {
			return null;
		}
	}

	function getServerIndex(serverId) {
		for(var i=0; i < serverList.length; i++) {
			if(serverList[i]['id'] == serverId) {
				return i;
			}
		}
		return null;
	}

	function getLastServerId() {
		if(serverList.length) {
			var lastServer = serverList[serverList.length - 1];
			return lastServer['id'];
		} else {
			return null;
		}
		
	}

	/** Returns Apps currently running in the server */
	function getServerActiveApps($server) {
		var apps = [];
		$server.find('.jsAppWrapper').each(function(){
			var prop = $(this).data('prop');
			apps.push(prop);
		});
		return apps;
	}

	function generateServerId() {
		return prefixServerId + Math.floor((Math.random() * 100) + 1) + Date.now();
	}

	function setTime() {
		$('.jsDashboard .jsAppWrapper').each(function(i){
			var prop = $(this).data('prop');
			var createdtime = prop['timestamp'];
			var timeDiff = Math.abs(createdtime - Date.now());
			var diffMin = Math.ceil(timeDiff / minute); 
			$(this).find('.jsTime').html(diffMin + ' min');
		});
	}
	function displayMessage(msg) {
		$('.jsDashboard').find('.jsMessage').html(msg).removeClass('hidden');
	}

	function hideMessage() {
		$('.jsDashboard').find('.jsMessage').addClass('hidden');
	}

	function initAppServer(appId) {
		appServers[appId] = {
			'serverIds' : [],
		}
	}

	function init() {
		activateDefaultActiveServers();
	}

	init();
});